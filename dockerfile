FROM openjdk:17
WORKDIR /app
ADD spring-boot-example/target/democrud-0.0.1-SNAPSHOT.jar ./democrud-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","democrud-0.0.1-SNAPSHOT.jar"]