package com.project.democrud.Service;

import com.project.democrud.DTO.UserDTO;
import com.project.democrud.DTO.UserSaveDTO;
import com.project.democrud.DTO.UserUpdateDTO;

import java.util.List;

public interface  UserService {
    String addUser(UserSaveDTO userSaveDTO);

    List<UserDTO> getAllUser();

    String updateuser(UserUpdateDTO userUpdateDTO);

    String deleteUser(int id);
}
