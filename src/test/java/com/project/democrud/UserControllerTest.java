package com.project.democrud;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.democrud.DTO.UserDTO;
import com.project.democrud.DTO.UserSaveDTO;
import com.project.democrud.DTO.UserUpdateDTO;
import com.project.democrud.Service.UserService;
import com.project.democrud.UserRepo.UserRepo;
import com.project.democrud.entity.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;
    private UserRepo userrepo;
    @MockBean
    private UserService userService;

    private UserDTO userDTO;

    private List<UserDTO> userDTOList;

    @BeforeEach
    void setUp() {
        userDTO = new UserDTO();
        userDTO.setId_user(1);
        userDTO.setNom("John");
        userDTO.setEmail("john@example.com");
        userDTO.setMobile(1234567890);

        userDTOList = Arrays.asList(userDTO);
    }


    @Test
    void updateUser() throws Exception {
        UserUpdateDTO userUpdateDTO = new UserUpdateDTO();
        userUpdateDTO.setId_user(1);
        userUpdateDTO.setNom("John Doe");
        userUpdateDTO.setEmail("johndoe@example.com");
        userUpdateDTO.setMobile(9);

        when(userService.updateuser(any(UserUpdateDTO.class))).thenReturn("1");

        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody = objectMapper.writeValueAsString(userUpdateDTO);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/v1/user/update")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("1"));
    }

    @Test
    void deleteUser() throws Exception {
        int id = 1;

        when(userService.deleteUser(id)).thenReturn("1");

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/user/delete/{id}", id))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("1"));
    }

    @Test
    void saveUser() throws Exception {
        UserSaveDTO userSaveDTO = new UserSaveDTO();
        userSaveDTO.setNom("John");
        userSaveDTO.setEmail("john@example.com");
        userSaveDTO.setMobile(1234567890);

        when(userService.addUser(any(UserSaveDTO.class))).thenReturn("1");

        ObjectMapper objectMapper = new ObjectMapper();
    }
    @Test
    public void testGetAllUser() {

        // Création de données de test
        User user1 = new User();
        user1.setId_user(1);
        user1.setImmatriculation("IMM123");
        user1.setNom("Doe");
        user1.setLieu_de_naissance("hhh");
        //... Initialiser les autres propriétés de l'objet User

        User user2 = new User();
        user2.setId_user(2);
        user2.setImmatriculation("IMM456");
        user2.setNom("Smith");
        //... Initialiser les autres propriétés de l'objet User

        List<User> userList = Arrays.asList(user1, user2);

        // Définir le comportement attendu du repository mock
        when(userrepo.findAll()).thenReturn(userList);

        // Appeler la méthode à tester
        List<UserDTO> result = userService.getAllUser();

        // Vérifier le résultat
        Assertions.assertEquals(2, result.size());
        Assertions.assertEquals("IMM123", result.get(0).getImmatriculation());
        Assertions.assertEquals("Doe", result.get(0).getNom());
        //... Vérifier les autres propriétés de UserDTO

        Assertions.assertEquals("IMM456", result.get(1).getImmatriculation());
        Assertions.assertEquals("Smith", result.get(1).getNom());
        //... Vérifier les autres propriétés de UserDTO
    }
}
